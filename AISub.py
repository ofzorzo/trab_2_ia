"""
This module collects command line arguments and prepares everything needed to run the simulator/game

Example:
    To quickly start the game and observe sensor readings:

        $ python AISub.py -c cave1 play
"""
import argparse
import pygame
import simulator
from controller1.controller import Controller
from controller2.controller import Controller as Controller2
import caves_config as cave


def play(cave_name: str) -> None:
    """
    Launches the simulator in a mode where the player can control each action with the arrow keys.

    :param str cave_name: Name of a cave, as defined in caves_config.py
    :param str b_type: String
    :rtype: None
    """
    game_state = simulator.Simulation(cave_name)
    frame_current = 0
    while frame_current <= 1100 and game_state.sub1.ontrack == True and game_state.sub1.oxigenio > 0:
        events = pygame.event.get()
        if len(events) > 0:
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        direction = 1
                        feedback = game_state.frame_step(direction)
                        print("sensors  " + str(feedback))
                        #print("features " + str(play_controller.compute_features(feedback)))
                        #print("score    " + str(play_controller.game_state.sub1.score))

                    if event.key == pygame.K_q:
                        exit()
                    if event.key == pygame.K_r:
                        game_state.reset()
        else:
            direction = 2
            feedback = game_state.frame_step(direction)
            print("sensors  " + str(feedback))
            #print("features " + str(play_controller.compute_features(feedback)))
            #print("score    " + str(play_controller.game_state.sub1.score))
        frame_current += 1
        print(frame_current)

    pass


def parser() -> (argparse.Namespace, list):
    """
    Parses command line arguments.

    :return: a tuple containing parsed arguments and leftovers
    """
    p = argparse.ArgumentParser(prog='AISub.py')
    mode_p = p.add_subparsers(dest='mode')
    mode_p.required = True
    p.add_argument('-c', nargs=1,
                   help='Specifies the cave you want to select; by default, cave0 will be used. '
                        'Check the \'caves_config.py\' file to see the available caves/create new ones.\n')
    p.add_argument('-f', nargs=1,
                   help='Specifies the file you want to load your Qtable.\n')
    p.add_argument('-e', nargs=1, type=int,
                   help="Specifies the number of episodes that will be executed in learning mode, the default "
                        "value is 100.\n")
    mode_p.add_parser('learn',
                      help='Starts %(prog)s in learning mode. This mode does not render the game to your screen, '
                           'resulting in '
                           'faster learning.\n')
    mode_p.add_parser('evaluate',
                      help='Starts %(prog)s in evaluation mode. This mode runs your AI with the Q-Table '
                           'passed as parameter \n')
    mode_p.add_parser('play',
                      help='Starts %(prog)s in playing mode. You can control each action of the sub using the arrow '
                           'keys of your keyboard.\n')
    mode_p.add_parser('comp',
                      help='Starts %(prog)s in competition mode.\n')
    arguments, leftovers = p.parse_known_args()
    p.parse_args()
    return arguments, leftovers

if __name__ == '__main__':

    args, trash = parser()

    # Selects cave; by default cave1 will be selected
    chosen_cave = cave.cave0
    if args.c is None:
        chosen_cave = cave.cave0
    else:
        for a_cave in cave.cave.cave_list:
            if args.c[0] == a_cave.name:
                chosen_cave = a_cave
    if args.f is None:
        table_path = None
    else:
        table_path = args.f[0]
    if args.e is None:
        number_of_episodes = 100
    else:
        number_of_episodes = int(args.e[0])

    # Starts simulator in play mode
    if str(args.mode) == 'play':
        simulator.show_simulation = True
        #simulation = simulator.Simulation(chosen_cave, bot_type)
        play(chosen_cave)
    # Starts simulator in evaluate mode
    elif str(args.mode) == 'evaluate':
        simulator.show_simulation = True
        ctrl = Controller(table_path)
        sim = simulator.Simulation(chosen_cave)
        sim.evaluate(ctrl)
    # Starts simulator in learn mode and saves the best results in a file
    elif str(args.mode) == 'learn':
        simulator.show_simulation = False
        simulation = simulator.Simulation(chosen_cave)
        ctrl = Controller(table_path)
        simulation.learn(ctrl, number_of_episodes)
    elif str(args.mode) == 'comp':
        simulator.show_simulation = True

        player_1_path = 'controller1/table.txt'
        player_2_path = 'controller2/table.txt'

        sub1_points = 0
        sub2_points = 0

        print("\n\nInicializando competição...\n")
        print("Player 1: submarino AMARELO")
        print("Player 2: submarino ROSA\n")
        #posição 1
        for current_cave in cave.cave.cave_list:
            #print("Player 1: posição 1")
            #print("Player 2: posição 2")
            player_1 = Controller(player_1_path)
            player_2 = Controller2(player_2_path)

            sim = simulator.Simulation(current_cave, player2 = True)
            sim.evaluate_comp(player_1, player_2)

            #print("Player 1 score: %d" % sim.sub1.score)
            #print("Player 2 score: %d" % sim.sub2.score)

            print("\n ||| Caverna: %s ||| \n" % current_cave.name)

            if sim.sub1.ontrack == False and sim.sub2.ontrack == False:
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")
                sub2_points += 1
                sub1_points += 1
            elif sim.sub1.oxigenio <= 0 and sim.sub2.oxigenio <= 0:
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")
                sub2_points += 1
                sub1_points += 1       
            elif sim.sub1.ontrack == False or sim.sub1.oxigenio <= 0:
                print("Ganhador: Player 2 -> Recebe 3 pontos\n")
                sub2_points += 3
            elif sim.sub2.ontrack == False or sim.sub2.oxigenio <= 0:
                print("Ganhador: Player 1 -> Recebe 3 Pontos\n")
                sub1_points += 3
            else:
                sub2_points += 1
                sub1_points += 1  
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")

            print("Player 1 points: %d" % sub1_points)
            print("Player 2 points: %d\n" % sub2_points)

            print("Trocando posições iniciais... \n")
            current_cave.sub1_position = (100, 350)
            current_cave.sub2_position = (100, 300)

            sim = simulator.Simulation(current_cave, player2 = True)
            sim.evaluate_comp(player_1, player_2)

            if sim.sub1.ontrack == False and sim.sub2.ontrack == False:
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")
                sub2_points += 1
                sub1_points += 1
            elif sim.sub1.oxigenio <= 0 and sim.sub2.oxigenio <= 0:
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")
                sub2_points += 1
                sub1_points += 1       
            elif sim.sub1.ontrack == False or sim.sub1.oxigenio <= 0:
                print("Ganhador: Player 2 -> Recebe 3 pontos\n")
                sub2_points += 3
            elif sim.sub2.ontrack == False or sim.sub2.oxigenio <= 0:
                print("Ganhador: Player 1 -> Recebe 3 Pontos\n")
                sub1_points += 3
            else:
                sub2_points += 1
                sub1_points += 1  
                print("Empate. Player 1 e Player 2 -> Recebem 1 ponto\n")

            print("Player 1 points: %d" % sub1_points)
            print("Player 2 points: %d\n" % sub2_points)

        print("\n ||| PONTUAÇÃO FINAL: |||\n")
        print("Player 1 points: %d" % sub1_points)
        print("Player 2 points: %d\n" % sub2_points)

        if sub1_points > sub2_points:
            print("Player 1 é o vencedor!!!")
        elif sub2_points > sub1_points:
            print("Player 2 é o vencedor!!!")
        else:
            print("Empate!!!")

