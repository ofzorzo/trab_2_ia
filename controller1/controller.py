# coding=utf-8
import interfaces as controller_template
import pickle
import math
import random
from itertools import product

from typing import Tuple, List, Any

ALPHA = 0.5
GAMMA = 1.0

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)

# Categories of sensors to determine additional informations
WATER = 0
OBSTACLE = 1
MONSTER = 2
OXYGEN = 3

sensors_categories = \
    [WATER,  # water_UP[1, 700]
     WATER,  # water_UP_RIGHT[1, 700]
     OBSTACLE,  # obstacle_UP[1, 700]
     OBSTACLE,  # obstacle_UP_RIGHT[1, 700]
     OBSTACLE,  # obstacle_AHEAD[1, 700]
     OBSTACLE,  # obstacle_DOWN_RIGHT[1, 700]
     OBSTACLE,  # obstacle_DOWN[1, 700]
     MONSTER,  # monster_UP[1, 200]
     MONSTER,  # monster_UP_RIGHT[1, 200]
     MONSTER,  # monster_AHEAD[1, 200]
     MONSTER,  # monster_DOWN_RIGHT[1, 200]
     MONSTER,  # monster_DOWN[1, 200]
     OXYGEN  # oxygen[1, 400]
     ]

INFINITY = 1000  # a value that is greater than all bounds of sensors

NEAR_THRESHOLD = 50
FAR_THRESHOLD = 400

LOW_THRESHOLD = 50
HIGH_THRESHOLD = 200

NEAR = 0
REGULAR = 1
FAR = 2

AIR = 1
NO_AIR = 0

LITTLE = 0
NORMAL = 1
A_LOT = 2

DISTANCE_INCREASE_REWARD = 10
OXIGEN_INCREASE_REWARD = 5
EXIST_AIR_REWARD = 1
STEP_REWARD = 0  # recompensa dada dependendo do quanto já executou
FINISH_REWARD = -1000
JUMP_REWARD = 0  # inicialmente nenhuma recompensa dada a ação, mantido aqui para possíveis mudanças experimentais
NOTHING_REWARD = 0

T = 5


class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        '''

        :param sensors: Sensors retrieved from the game state, the sensors will appear in the following order:
            0 -> water_UP [1, 700]
            1 -> water_UP_RIGHT [1, 700]
            2 -> obstacle_UP [1, 700]
            3 -> obstacle_UP_RIGHT [1, 700]
            4 -> obstacle_AHEAD [1, 700]
            5 -> obstacle_DOWN_RIGHT [1, 700]
            6 -> obstacle_DOWN [1, 700]
            7 -> monster_UP [1, 200]
            8 -> monster_UP_RIGHT [1, 200]
            9 -> monster_AHEAD [1, 200]
            10 -> monster_DOWN_RIGHT [1, 200]
            11 -> monster_DOWN [1, 200]
            12 -> oxygen[1, 400]

        :return list of the features that will be used to learn the parameters:
        '''
        normalized_sensors = self.normalize_sensor_domains(self.sensors)
        # smaller distance to object down that could damage the submarine
        F1 = min(normalized_sensors[6], normalized_sensors[11])
        # smaller distance to object up that could damage the submarine
        F2 = min(normalized_sensors[2], normalized_sensors[7])
        # smaller distance to object down right that could damage the submarine
        F3 = min(normalized_sensors[5], normalized_sensors[10])
        # smaller distance to object up right that could damage the submarine
        F4 = min(normalized_sensors[3], normalized_sensors[8])
        # smaller distance to object ahead that could damage the submarine
        F5 = min(normalized_sensors[4], normalized_sensors[9])

        # water distance -> maybe this feature will become better if not modeled this way because it is not a dangerous
        # situation to get to close to water, the idea is that if it is closer the action to go up should be taken more
        # seriously
        F6 = normalized_sensors[0]
        # same idea of F6
        F7 = normalized_sensors[1]

        # the oxigen level
        F8 = normalized_sensors[12]

        return [F1, F2, F3, F4, F5, F6, F7, F8]

    def normalize_sensor_domains(self, sensors: list) -> list:
        normalized_sensors = []
        for i in range(len(sensors)):
            s = sensors[i]
            if (sensors_categories[i] == OBSTACLE and sensors[i] == 700) or \
                    (sensors_categories[i] == MONSTER and sensors[i] == 200):
                s = INFINITY
            normalized_sensors.append(s)
        return normalized_sensors

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features
        :return: A tuple containing the discretized features
        """
        """
            A discretização vai ser feita da seguinte forma: 
            F1, F2, F3, F4, F5  : relacionadas a distância para obstáculos, serão discretizadas da seguinte forma:
            Existirão três níveis indicando que o obstáculo está perto (NEAR), regular (REGULAR) ou longe (FAR) do submarino. 
            Esses níveis serão coordenados pelos valores NEAR_THRESHOLD e FAR_THRESHOLD

            F6, F7 : relacionadas a distância de ar acima serão discretizadas em apensa dois níveis, no caso, se existe
            (AIR) ou se não existe(NO_AIR) ar em cima

            F8 : será discretizado em 3 níveis : pouco (LITTLE), razoável(NORMAL) e muito(A_LOT) controlados pelos valores LOW_THRESHOLD e 
            HIGH_THRESHOLD

        """
        discrete_features = []

        for i in range(5):
            df = REGULAR
            if features[i] >= FAR_THRESHOLD:
                df = FAR
            elif features[i] <= NEAR_THRESHOLD:
                df = NEAR
            discrete_features.append(df)

        for i in range(5, 7):
            df = NO_AIR
            if features[i] < 700:
                df = AIR
            discrete_features.append(df)

        df = NORMAL
        if features[7] <= LOW_THRESHOLD:
            df = LITTLE
        elif features[7] >= HIGH_THRESHOLD:
            df = A_LOT
        discrete_features.append(df)

        return discrete_features

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return (3, 3, 3, 3, 3, 2, 2, 3)

    def get_current_state(self) -> Tuple:
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return features

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as
        parameter.
        :param discretized_features
        :return: unique key
        """
        terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return:
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]


class QTable(controller_template.QTable):
    def __init__(self, table={}):
        if bool(table) == False:
            self.table_dict = {}
            all_states = State.enumerate_all_possible_states()
            for state in all_states:
                key = State.get_state_id(state)
                self.table_dict[key] = [0.0, 0.0]  # inicia as preferencias por uma ação em 0
        else:
            self.table_dict = table

    def get_q_value(self, key: State, action: int) -> float:
        key = State.get_state_id(key.get_current_state())
        return self.table_dict[key][action - 1]

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        key = State.get_state_id(key.get_current_state())
        self.table_dict[key][action - 1] = new_q_value

    def save(self, path: str, *args) -> None:
        with open(path, 'wb') as output:
            pickle.dump(self.table_dict, output, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(path: str) -> "QTable":
        with open(path, 'rb') as input:
            loaded_table_dict = pickle.load(input)
        return QTable(loaded_table_dict)


class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_episode: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get in new_state
        :param n_steps: number of steps the sub has taken so far in the current race
        :param end_of_episode: boolean indicating if an episode has ended # Nota : em todas as chamadas para essa função end_of_episode é False
        :return: The reward to be given to the agent
        """

        '''Essa é bastante importante para o aprendizado adequado e seu cálculo provavelmente vai ser o que mais vai 
        precisar de ajustes experimentais'''

        ### REWARD PELA ALTERAÇÃO DO ESTADO CONSIDERANDO AS FEATURES DISCRETIZADAS
        reward = 0

        old_discr_features = old_state.discretize_features(old_state.compute_features())
        new_discr_features = new_state.discretize_features(new_state.compute_features())

        # diff features contém a diferença das features discretizadas do estado anterior para o atual
        diff_features = new_discr_features
        for f in range(8):
            diff_features[f] -= old_discr_features[f]

        for f in range(5):
            # se diferença é negativa está mais perto de obstáculo do que antes logo diminui recompensa
            reward += DISTANCE_INCREASE_REWARD * diff_features[f]

        for f in range(5, 7):
            reward += EXIST_AIR_REWARD * diff_features[f]

        reward += OXIGEN_INCREASE_REWARD * diff_features[7]

        ### REWARD POR QUANTO O AGENTE JÁ PROGREDIU
        reward += STEP_REWARD * n_steps

        ### REWARD PELA ÚLTIMA AÇÃO TOMADA : ACHO QUE ISSO VAI TER QUE SER AJUSTADO EXPERIMENTALMENTE POIS AS AÇÕES
        ### NÃO TEM CUSTO E FAZER O AGENTE PREFERIR UMA PELA OUTRA PARECE PERIGOSO, A MENOS QUE CONSIDERAMOS QUE
        ### O AGENTE PRECISA MUITAS VEZES FAZER NADA E POUCAS VEZES PULAR, NO ENTANTO ESSES ESTADOS JÁ ESTARIAM
        ### RECOMPENSADOS PELAS DIFERENÇAS DE FEATURES ACIMA

        if action == 1:  # jump
            reward += JUMP_REWARD
        if action == 2:
            reward += NOTHING_REWARD
        if end_of_episode:
            reward+= FINISH_REWARD
        return reward

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        gamma_term = GAMMA * max(self.q_table.get_q_value(new_state, 0), self.q_table.get_q_value(new_state, 1))
        new_value = (1 - ALPHA) * self.q_table.get_q_value(old_state, action) + ALPHA * (reward + gamma_term)
        self.q_table.set_q_value(old_state, action, new_value)

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the submarine must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the submarine
        :param episode_number: current episode/race during the training period
        :return: The action the submarine chooses to execute
        """
        global T
        action_up = self.q_table.get_q_value(new_state, 1) / T
        action_nothing = self.q_table.get_q_value(new_state, 2) / T
        if action_up > action_nothing and action_up != 0:
            action_up = action_up / abs(action_up)
            action_nothing = action_nothing / abs(action_up)
        else:
            if action_nothing != 0:
                action_up = action_up / abs(action_nothing)
                action_nothing = action_nothing / abs(action_nothing)
        sum_actions = math.exp(action_up) + math.exp(action_nothing)
        if sum_actions == 0:
            print("T=", T)
            print("ACTION 1=", self.q_table.get_q_value(new_state, 1))
            print("ACTION 1=", self.q_table.get_q_value(new_state, 2))
        prob_up = math.exp(action_up) / sum_actions
        prob_nothing = math.exp(action_nothing) / sum_actions
        random_factor = random.random()
        action = 1 if random_factor <= prob_up else 2
        self.q_table.save('qtable.txt')
        #print(T)
        saveT = T
        T = T * 0.9999
        if T < 0.01:
            T = saveT
        return action